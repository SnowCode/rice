# My new setup
Just remade my setup because I wanted something cleaner. Was kind of a pain because of some update in dwm's code, I had to patch it by hand.

## Features
* Dwm (nice, minimal, easy to use and understand, not a lot of shortcuts to use it)
    * Patched with fullgaps, moveresize and azerty
    * Modified for using windows key as MOD, slightly thicker borders and bar hidden by default
* Alacritty with usage of Fantasque Sans Mono font and slightly tranparent.
* 80 different wallpapers and automatic synchronization of the color scheme of alacritty with the wallpaper using pywal.
* Qutebrowser with darkmode, adblocker, noscript, redirection of videos to mpv, some fingerprinting protection things and a password manager
* Usage of pijulius's picom fork

## Installation
The following snippet should install everything you need if you're on arch linux:

```bash
sudo pacman -Sy xorg libxinerama libx11 libxft dmenu libxkbcommon alacritty python-pywal qutebrowser yay stow
yay picom-pijulius-git
```

Now you can use stow to install the packages from here:

```bash
git clone https://codeberg.org/SnowCode/rice
cd rice/
stow *
# or, if you only want one package (i.e alacritty): 
stow alacritty
```
