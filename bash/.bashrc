export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
export TERM=linux
export EDITOR=nano
export LC_ALL=fr_BE.UTF-8
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export PATH=$PATH:~/.local/bin

alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias irc='ssh debian@51.178.55.122 -t tmux attach -t weechat'
alias vps='ssh debian@51.178.55.122'
alias mpv='mpv --hwdec=vaapi'

cat ~/.cache/wal/sequences
echo -ne "\x1b[3 q" #echo -ne "\x1b[6 q"

